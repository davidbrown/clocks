def Settings(**kwargs):
    return {
        'flags': [
            '-x', 'c++',
            '-Ilib/include',
            '-std=c++17',
            '-Wall', '-Wextra', '-Wpedantic'
        ]
    }
