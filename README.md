Clocks
======

A collection of `std::chrono` based clocks.

`application_clock`
-----------------
It can interesting to track the total runtime of an application, or to get an
idea of how long the initialisation phase of an application is.
`application_clock` uses the `std::chrono::steady_clock` and an internal epoch
to allow such measurements to be made.

`application_clock` meets the requirements for TrivialClock.

The only required step is to initialize the epoch as early as possible:
```c++
#include <clocks/application_clock.hpp>

int main(int argc, char* argv[])
{
	// Make sure to call this early!
	clocks::application_clock::initialize_epoch();

	// Some time consuming tasks

	auto const boot_time = clocks::application_clock::now();
	auto const duration = boot_time.time_since_epoch();

	std::cout << "Init time: " << duration.count() << "us\n";

	return 0;
}
```

Build Instructions
------------------
The clocks are all provided as libraries that can be either statically linked,
or shared (user's choice).

To build simply run the following:
```bash
mkdir build
cd build
cmake ..
make
```

### Requirements
* g++/clang++ (ISO C++14 or later compliant)
* cmake version 3.8 or higher
* make

### Configuration Options
There are test applications that demonstrate the basic usage of these clocks.
These are not built by default. To build them, ensure that the option
`BUILD_TEST_APPLICATIONS` is set to `ON` when invoking cmake:

```bash
cmake <path/to/repository> -DBUILD_TEST_APPLICATIONS=ON
```

The test applications can be found in the build directories `test_app`
subfolder.

