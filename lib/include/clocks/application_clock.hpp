#pragma once

/**
 * MIT License
 *
 * Copyright (c) 2019 David Brown <d.brown@bigdavedev.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "clock_traits.hpp"

#include <chrono>

namespace clocks
{
	/**
	 * An std::chrono based clock to measure time since the application
	 * started.
	 *
	 * @remarks Meets the requirements of TrivialClock
	 */
	class application_clock
	    : clock_traits<std::chrono::steady_clock, std::chrono::microseconds>
	{
	private:
		static_assert(internal_clock::is_steady,
		              "internal_clock must be a steady_clock");

	public:
		/**
		 * Set the epoch of the application at startup.
		 *
		 * Making this one of the earliest functions called in main will ensure
		 * an accurate epoch for the application.
		 */
		static void initialize_epoch();

		static time_point now() noexcept;

	private:
		static time_point epoch;
	};
} // namespace clocks

