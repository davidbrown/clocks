#pragma once

/**
 * MIT License
 *
 * Copyright (c) 2019 David Brown <d.brown@bigdavedev.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <chrono>
#include <utility>

namespace clocks
{
	template <typename Duration,
	          typename Clock = std::chrono::steady_clock>
	struct profile
	{
		static_assert(Clock::is_steady,
		              "Clock must be steady to ensure accuracy");

		template <typename F, typename... Args>
		constexpr static auto execution_of(F&& function, Args&&... args)
		{
			auto start = Clock::now();
			std::forward<decltype(function)>(function)(
			    std::forward<Args>(args)...);
			auto end = Clock::now();
			return std::chrono::duration_cast<Duration>(end - start);
		}
	};
} // namespace clocks

