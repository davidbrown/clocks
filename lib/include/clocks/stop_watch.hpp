#pragma once

/**
 * MIT License
 *
 * Copyright (c) 2019 David Brown <d.brown@bigdavedev.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "clock_traits.hpp"

#include <chrono>

namespace clocks
{
	/**
	 * An std::chrono based clock to measure time elapsed between start and
	 * stop invocations. Also supports lap-timing.
	 *
	 * @remarks Does not meet the requirements of TrivialClock
	 */
	class stop_watch
	    : clock_traits<std::chrono::steady_clock, std::chrono::microseconds>
	{
		static_assert(internal_clock::is_steady,
		              "internal_clock must be a steady_clock");

	private:
		enum class state
		{
			stopped,
			started
		};

	public:
		stop_watch();
		stop_watch(stop_watch const&) = delete;
		stop_watch(stop_watch&&)      = delete;
		stop_watch& operator=(stop_watch const&) = delete;
		stop_watch& operator=(stop_watch&&) = delete;
		~stop_watch()                       = default;

		void     start() noexcept;
		void     stop() noexcept;
		duration elapsed() const noexcept;
		duration lap() noexcept;

	private:
		state      current_state;
		time_point epoch;
		duration   current_lap;
	};
} // namespace clocks

