/**
 * MIT License
 *
 * Copyright (c) 2019 David Brown <d.brown@bigdavedev.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "clocks/stop_watch.hpp"

namespace clocks
{
	stop_watch::stop_watch() : epoch{}, current_lap{} {}

	void stop_watch::start() noexcept
	{
		if (current_state != state::started)
		{
			current_state = state::started;
			epoch         = internal_clock::now();
		}
	}

	void stop_watch::stop() noexcept
	{
		if (current_state != state::stopped)
		{
			current_state = state::stopped;
			current_lap   = duration{};
		}
	}

	stop_watch::duration stop_watch::elapsed() const noexcept
	{
		auto const elapsed_time =
		    std::chrono::duration_cast<duration>(internal_clock::now() - epoch);
		return std::chrono::duration_cast<duration>(elapsed_time);
	}

	stop_watch::duration stop_watch::lap() noexcept
	{
		if (current_state == state::started)
		{
			current_lap = elapsed() - current_lap;
		}
		return current_lap;
	}
} // namespace clocks
