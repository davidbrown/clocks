/**
 * MIT License
 *
 * Copyright (c) 2019 David Brown <d.brown@bigdavedev.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#include "clocks/stop_watch.hpp"

#include <iostream>
#include <thread>

using namespace std::chrono_literals;

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
	clocks::stop_watch stop_watch{};

	stop_watch.start();
	std::this_thread::sleep_for(1234ms);
	std::cout << "Lap 1: " << stop_watch.lap().count() << "us\n";

	std::this_thread::sleep_for(1432ms);
	std::cout << "Lap 2: " << stop_watch.lap().count() << "us\n";

	std::cout << "Total runtime: " << stop_watch.elapsed().count() << "us\n";

	stop_watch.stop();
	std::cout << "Lap 3: " << stop_watch.lap().count() << "us\n";
	return 0;
}

